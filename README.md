# Live Healthy

Finds exercises for your meal. If you want to know how many calories
one has to burn for a hamburger or half of pizza, you are at the right place!

## Running

You need to have Ruby and some nutrition and exercise data. The sample data provided
may not be enough for you.

Use `data/food.csv` and `data/exercises.csv` to add more items. If there are multiple food
or exercise records with same calories, the last one will be used. Calorie values are in "kilocalorie"
and should be an integer number.

Once the data is set, run it:

```bash
./healthy --advice minimal
```

It will return a minimal combination of food and exercises, that will compensate each other.
