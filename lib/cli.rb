# frozen_string_literal: true

require 'set'
require 'optionparser'

# Responsible for parsing arguments given to a command
# line script.
#
# Will return either OK or Error value object for a
# high-level script to base decisions on.
class Cli
  VALID_TYPES = Set.new([:minimal])
  DEFAULTS = {
    food: File.join(
      File.expand_path('../..', __FILE__),
      'data/food.csv'
    ),
    exercises: File.join(
      File.expand_path('../..', __FILE__),
      'data/exercises.csv'
    )
  }.freeze

  OK = Struct.new(:advice, :food_file_path, :exercises_file_path) do
    def ok?
      true
    end
  end

  Error = Struct.new(:error_messages) do
    def ok?
      false
    end
  end

  def self.arguments(args)
    ok = {}
    errors = []

    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: ./healthy --advice=type ' \
                    ' [--food=path/to/food.csv] [--exercises=path/to/exercises.csv]'

      opts.on('--advice=NAME', 'Type of food to exercise advice') do |type|
        if VALID_TYPES.include?(type.to_sym)
          ok[:advice] = type.to_sym
        else
          errors << "Advice '#{type}' is not valid." \
                    " Choose one of: #{VALID_TYPES.to_a.join(', ')}"
        end
      end

      opts.on('--food=PATH',
              "Path to the food data file. Default: #{DEFAULTS[:food]}") do |path|
        if File.exist?(path)
          ok[:food] = path
        else
          errors << "File '#{path}' does not exist"
        end
      end

      opts.on('--exercises=PATH',
              "Path to the exercises data file. Default: #{DEFAULTS[:exercises]}") do |path|
        if File.exist?(path)
          ok[:exercises] = path
        else
          errors << "File '#{path}' does not exist"
        end
      end

      opts.on('-h', '--help', 'Prints this help') do
        puts opts
        exit
      end
    end

    opt_parser.parse!(args)

    if errors.empty? && !ok[:advice].nil?
      ok[:food] ||= DEFAULTS[:food]
      ok[:exercises] ||= DEFAULTS[:exercises]

      OK.new(ok[:advice], ok[:food], ok[:exercises])
    else
      Error.new(errors)
    end
  end
end
