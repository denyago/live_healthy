# frozen_string_literal: true

# Correlates food and exercises to find proper
# combination of them.
class Correlator
  attr_reader :food, :exercises, :food_keys, :exercises_keys

  def initialize(food_in, exercises_in)
    @food = food_in
    @exercises = exercises_in
    @food_keys = food_in.keys.sort
    @exercises_keys = exercises_in.keys.sort
  end

  def find_exercises
    correlation = one_on_one

    if correlation.empty?
      many_to_many
    else
      correlation
    end
  end

  private

  def one_on_one
    my_food_key = food_keys.find { |k| exercises.key?(k) }

    if my_food_key.nil?
      {}
    else
      {
        food: { my_food_key => food[my_food_key] },
        exercises: { my_food_key => exercises[my_food_key] }
      }
    end
  end

  def many_to_many
    solutions = []
    exercises_items = exercises_keys.map { |k| { weight: k, price: 1 } }
    food_items = food_keys.map { |k| { weight: k, price: 1 } }

    food_keys.each do |calories|
      result = fill_the_bag(calories, exercises_items)
      solutions << { type: :food, calories: [calories], items: result }
    end

    exercises_keys.each do |calories|
      result = fill_the_bag(calories, food_items)
      solutions << { type: :exercises, calories: [calories], items: result }
    end

    food_keys.size.times do |t|
      calories = food_keys[0, t + 1].sum
      next if calories == 0

      result = fill_the_bag(calories, exercises_items)
      solutions << { type: :food, calories: food_keys[0, t + 1], items: result }
    end

    exercises_keys.size.times do |t|
      calories = exercises_keys[0, t + 1].sum
      next if calories == 0

      result = fill_the_bag(calories, food_items)
      solutions << { type: :exercises, calories: exercises_keys[0, t + 1], items: result }
    end

    compensate_each_other = solutions.select do |s|
      s[:calories].sum == s[:items].map { |i| i[:weight] }.sum
    end

    if compensate_each_other.empty?
      {}
    else
      solution = compensate_each_other.first

      if solution[:type] == :food
        {
          food: solution[:calories].inject({}) { |memo, k| memo.merge(k => food[k]) },
          exercises: solution[:items].inject({}) { |memo, i| memo.merge(i[:weight] => exercises[i[:weight]]) }
        }
      else
        {
          food: solution[:items].inject({}) { |memo, i| memo.merge(i[:weight] => food[i[:weight]]) },
          exercises: solution[:calories].inject({}) { |memo, k| memo.merge(k => exercises[k]) }
        }
      end
    end
  end

  # Borrowed from here https://github.com/mertbulan/knapsack-problem/blob/master/lib/knapsack.rb
  def fill_the_bag(capacity, items)
    return 0 if capacity == 0 || items.empty?

    @capacity = capacity
    @weights  = items.collect { |item| item[:weight] }.insert(0, 0)
    @prices   = items.collect { |item| item[:price] }.insert(0, 0)
    @bag      = (items.size + 1).times.map { [0] * (capacity + 1) }

    (1..items.size).each do |item|
      (1..capacity).each do |weight|
        last_item = @bag[item - 1]
        @bag[item][weight] = if @weights[item] > weight
                               last_item[weight]
                             else
                               [last_item[weight - @weights[item]] + @prices[item], last_item[weight]].max
        end
      end
    end
    inside_bag(@bag, @capacity, items)
  end

  def inside_bag(bag, capacity, items)
    weight = capacity

    items.reverse.select.with_index do |item, index|
      i = items.size - index
      bag[i][weight] > bag[i - 1][weight] && weight -= item[:weight]
    end
  end
end
