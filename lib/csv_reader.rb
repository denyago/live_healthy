# frozen_string_literal: true

require 'csv'

# A simple wrapper around CSV library to provide a
# hash map of calories and type of food / exercise.
class CsvReader
  def self.read(file_path)
    CSV.foreach(file_path).inject({}) do |memo, row|
      if row.first.to_i.zero?
        memo
      else
        memo.merge(row.first.to_i => row.last)
      end
    end
  end
end
