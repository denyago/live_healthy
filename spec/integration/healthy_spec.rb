# frozen_string_literal: true

RSpec.describe './integration' do
  it 'returns an answer' do
    food_path = File.join(
      File.expand_path('../..', __FILE__),
      'sample_data/integration_food.csv'
    )
    exercises_path = File.join(
      File.expand_path('../..', __FILE__),
      'sample_data/integration_exercises.csv'
    )
    script_path = File.join(
      File.expand_path('../../..', __FILE__),
      'healthy'
    )

    # rubocop:disable LineLength
    result = `#{script_path} --advice minimal --food #{food_path} --exercises #{exercises_path}`
    expect(result.strip).to eq 'Doing "Run 10 min." will totally burn "Chips, 100g"'
    # rubocop:enable LineLength
  end

  it 'returns no answer' do
    food_path = File.join(
      File.expand_path('../..', __FILE__),
      'sample_data/extra_sample.csv'
    )
    exercises_path = File.join(
      File.expand_path('../..', __FILE__),
      'sample_data/extra_sample.csv'
    )
    script_path = File.join(
      File.expand_path('../../..', __FILE__),
      'healthy'
    )

    # rubocop:disable LineLength
    result = `#{script_path} --advice minimal --food #{food_path} --exercises #{exercises_path}`
    expect(result.strip).to eq 'Could not find combination of food and exercises'
    # rubocop:enable LineLength
  end
end
