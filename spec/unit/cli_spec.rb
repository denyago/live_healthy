# frozen_string_literal: true

require 'lib/cli'

RSpec.describe Cli do
  describe '.arguments' do
    let(:prefix) { Dir.pwd }

    context 'when the input contains mandatory options' do
      it 'returns an "OK" options object' do
        expect(
          described_class.arguments(['--advice', 'minimal'])
        ).to eq Cli::OK.new(:minimal, prefix + '/data/food.csv', prefix + '/data/exercises.csv')
      end
    end

    context 'when the input contains extra options' do
      it 'returns an "OK" options object' do
        expect(
          described_class.arguments(
            ['--advice', 'minimal',
             '--food', prefix + '/spec/sample_data/sample.csv',
             '--exercises', prefix + '/spec/sample_data/extra_sample.csv']
          )
        ).to eq Cli::OK.new(:minimal,
                            prefix + '/spec/sample_data/sample.csv',
                            prefix + '/spec/sample_data/extra_sample.csv')
      end
    end

    context 'when the input has a invalid advice type' do
      it 'returns an "Error" options object' do
        expect(
          described_class.arguments(['--advice', 'a-miracle'])
        ).to eq Cli::Error.new(["Advice 'a-miracle' is not valid. Choose one of: minimal"])
      end
    end

    context 'when the input has a non-existant file' do
      it 'returns an "Error" options object' do
        expect(
          described_class.arguments(
            ['--advice', 'minimal',
             '--food', prefix + '/i-do-no-exist.csv']
          )
        ).to eq Cli::Error.new(["File '#{prefix + '/i-do-no-exist.csv'}' does not exist"])
      end
    end
  end
end
