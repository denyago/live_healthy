# frozen_string_literal: true

require 'lib/correlator'

RSpec.describe Correlator do
  describe '.find_exercises' do
    context 'when there is only one exercise and one food matches' do
      it 'returns that exercise and food' do
        food = {
          700 => 'Hamburger, 400g',
          100 => 'Chips, 100g'
        }

        exercises = {
          100 => 'Run 10 min.',
          200 => 'Run 20 min.'
        }

        expect(
          described_class.new(food, exercises).find_exercises
        ).to eq(food: { 100 => 'Chips, 100g' },
                exercises: { 100 => 'Run 10 min.' })
      end
    end

    context 'when there are multiple matching foods and exercises' do
      it 'returns the samllest one' do
        food = {
          100 => 'Chips, 100g',
          50 => 'Candy, 10g'
        }

        exercises = {
          100 => 'Run 10 min.',
          50 => 'Squats, 25'
        }

        expect(
          described_class.new(food, exercises).find_exercises
        ).to eq(food: { 50 => 'Candy, 10g' },
                exercises: { 50 => 'Squats, 25' })
      end
    end

    context 'when there are no matching foods and exercises one on one' do
      it 'returns the combination of multiple items' do
        food = {
          100 => 'Chips, 100g',
          50 => 'Candy, 10g'
        }

        exercises = {
          150 => 'Run with weights, 10 min.',
          60 => 'Squats, 25'
        }

        expect(
          described_class.new(food, exercises).find_exercises
        ).to eq(food: { 100 => 'Chips, 100g', 50 => 'Candy, 10g' },
                exercises: { 150 => 'Run with weights, 10 min.' })
      end
    end

    context 'when there are no matching foods and exercises' do
      it 'returns nothing' do
        food = {
          51 => 'Candy, 10g'
        }

        exercises = {
          99 => 'Run 10 min.'
        }

        expect(
          described_class.new(food, exercises).find_exercises
        ).to eq({})
      end
    end
  end
end
