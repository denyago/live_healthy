# frozen_string_literal: true

require 'lib/csv_reader'

RSpec.describe CsvReader do
  describe '.read' do
    it 'returns a hash structure from a CSV file' do
      file_path = File.join(
        File.expand_path('../..', __FILE__),
        'sample_data/sample.csv'
      )

      expect(
        described_class.read(file_path)
      ).to eq(
        10 => 'Activity, 1',
        20 => 'Activity, 2',
        30 => 'Activity, 30 again'
      )
    end
  end
end
